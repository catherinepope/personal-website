---
title: About
permalink: /about/
---

Hello :wave: I’m Catherine Pope, a writer, academic coach, and trainer.

I enjoy mastering difficult concepts and helping other people understand them.

With an extensive background in industry and academia, I’ve created training materials in a range of formats, including user guides, books, and video-based courses.

During my long career, I've worked as a web developer, IT manager, technical writer, publisher, and university lecturer. Yes, it's quite a range, but all these roles bring together my love of learning, teaching, and technology.

I recently left the tech sector to work for [Research Coach](https://www.researchcoach.co.uk), where I'm coaching and training academics, from PhD researchers through to professors. I'm especially interested in using AI and other assistive technologies to streamline research and writing. All views here are my own and don't represent my employer.

In the tiny amount of time that’s left, I manage [Victorian Secrets](https://www.victoriansecrets.co.uk), a micro press dedicated to books from and about the nineteenth century.

I live in Brighton on the south coast of England with my partner Tanya, a stripy cat, and thousands of books.

### Recent Training

- Certified Kubernetes Administrator (A Cloud Guru)
- Docker Certified Associate (A Cloud Guru)
- Intermediate/Advanced Technical Communication (Cherryleaf)
- Technical Author Writing Course (Cherryleaf)
- Full-Stack Engineer (Codecademy)

You can find out much more about my employment history, qualifications, and interests on [my LinkedIn profile](https://www.linkedin.com/in/drcatherinepope/).

I'm a certified Member of the [Institute of Scientific and Technical Communicators](https://istc.org.uk).

![Logo of Institute of Scientific and Technical Communicators](/images/istc-certified-member-landscape-small.png)

### About this Site

This site is written in Markdown and built with Hugo, using the [Mainroad theme](https://themes.gohugo.io/themes/mainroad/). Once committed to my repo, the site is automatically built deployed through GitLab pages.
