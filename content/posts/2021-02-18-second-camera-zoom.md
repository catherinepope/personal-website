---
title:  "How to Use a Second Camera in Zoom"
date:   2021-02-18 10:19:28 +0100
authorbox: true
categories: tutorials

---

In this 90-second demo, I'll show you a little-known setting in Zoom that allows you to use a second camera. This is great for sharing objects and techniques off-screen, alongside your talking head.

{{< youtube Te7-NYFgXSo >}} 

You might also be interested in my post on [How to Turn Your Phone into a Webcam](/tutorials/2020/07/03/turn-phone-into-webcam.html).