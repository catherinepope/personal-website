---
title:  "Conquer Boring Tasks with the Pomodoro Technique"
date:   2021-02-04 10:19:28 +0100
authorbox: true
categories: tutorials
---

We all have tasks we avoid doing. For some, it’s writing blog posts, while others struggle with keeping on top of email. Nearly everyone, though, will do anything to avoid accounts. In this short video, I'll introduce you to the [Pomodoro Technique](https://francescocirillo.com/pages/pomodoro-technique), an effective way of conquering those boring jobs.

{{< youtube fD_2MehJlHU >}} 