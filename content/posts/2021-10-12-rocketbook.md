---
title:  "Capture Handwritten Notes with a Rocketbook"
date:   2021-02-11 10:19:28 +0100
categories: tutorials
---
There are lots of great note-taking apps out there, but sometimes you just want to scribble with a pen and paper. In this short demo, I'll show how the [Rocketbook](https://amzn.to/3dAXJYq)[^1] gives you the best of analogue and digital.

{{< youtube ou5mQMq_VKM >}} 

[^1]:This is an Amazon affiliate link, on which I receive a small commission. I'm not associated with Rocketbook in any way - I'm just a very enthusiastic owner of several!