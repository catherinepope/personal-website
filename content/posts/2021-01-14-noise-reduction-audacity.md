---
title:  "Easy Noise Reduction with Audacity"
date:   2021-01-14 10:19:28 +0100
authorbox: true
categories: tutorials

---

Once you get into audio and video recording, background noise will drive you absolutely mad. Although there are lots of solutions out there, most of them are very complicated and often expensive, too. My name is Catherine. In this short video, I'll show you a simple method for removing background noise in a free tool called [Audacity](https://www.audacityteam.org/).

{{< youtube q0CLvi6HWT8 >}} 