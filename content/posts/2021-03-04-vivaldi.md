---
title:  "How to Accelerate Your Learning with Vivaldi"
date:   2021-03-04 10:19:28 +0100
authorbox: true
categories: tutorials

---

It's hard to manage multiple web-based tools, especially for online learning. With the [Vivaldi browser](https://vivaldi.com/), you can display tabs in panes. I'll show you how in this speedy demo.

{{< youtube KI3-WWA9tFw >}} 