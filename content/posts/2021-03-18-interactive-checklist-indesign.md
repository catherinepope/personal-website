---
title:  "How to Create an Interactive Checklist in InDesign"
date:   2021-03-18 10:19:28 +0100
authorbox: true
categories: tutorials

---

In this short tutorial, I'll show you how you can create a checklist in InDesign that's both interactive and printable. I'm assuming you already know the basics of InDesign. If not, I recommend the [InDesign Essentials course]( https://skillshare.eqcm.net/6bbxDq) on Skillshare (affiliate link).

{{< youtube uKBZ3PedL08 >}} 