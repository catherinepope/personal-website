---
title:  "How to Capture Web Snippets with Hypothesis"
date:   2021-04-08 10:19:28 +0100
authorbox: true
categories: tutorials
---

In this demo, I'll show you [Hypothesis](https://hypothes.is), a free Chrome extension for managing web snippets and annotations. It also syncs with [Readwise.io](https://readwise.io), which means you can keep a wide range of content all in one place.

{{< youtube 9lFpNkNJu0E >}} 